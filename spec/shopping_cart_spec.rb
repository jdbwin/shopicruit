require 'spec_helper'

describe ShoppingCart do
  before :each do
    @shopping_cart = ShoppingCart.new "Item", "Max_Weight"
  end

  describe "#new" do
    it "takes two parameters and return ShoppingCart object" do
      expect(@shopping_cart).to be_an_instance_of ShoppingCart
    end
  end

  describe "#show_cart" do
    it "displays the contents of the cart" do
      expect {@shopping_cart.show_cart}.to output('Hello').to_stdout
    end
  end
end
