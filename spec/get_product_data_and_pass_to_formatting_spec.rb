require 'spec_helper'

describe GetProductData do

  it 'receive JSON' do
    expect{(GetProductData::PRODUCT_DATA).to_not raise_error}
  end

  it 'returns a hash' do
    expect(GetProductData::PRODUCT_DATA).to be_a(Hash)
  end

  it 'should not be empty' do
    expect(GetProductData::PRODUCT_DATA).not_to be_empty
  end

end
