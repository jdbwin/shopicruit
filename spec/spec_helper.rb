RSpec.configure do |config|
  config.color = true
  config.tty = true
  config.formatter = :documentation
end

require_relative '../bin/Shopicruit'
require_relative '../lib/shopping_cart'
require_relative '../lib/get_product_data.rb'
