require_relative 'lib/get_product_data'
require_relative 'lib/product_list'
require_relative 'lib/shopping_cart'

class Shopicruit

  def initialize
    @product_data = GetProductData::PRODUCT_DATA["products"]
    @shopping_cart = ShoppingCart.new
    @product_list = ProductList.new(@product_data, @shopping_cart)
  end

  def menu
    command = ""

    until command.downcase == "quit" || command == "exit"

      puts "", "Menu", "", "1. Display products",
        "2. View Shopping Cart",
        "or, type 'Quit' to quit", ""
      print '>> '

      command = gets.chomp

      display_product_list if command == "1"
      display_shopping_cart if command == "2"
    end

  end

  def display_product_list
    product_type_to_view = "" 
    puts "", "Product Types: "
    @product_list.display_product_list

    until @product_list.generate_product_list.include? product_type_to_view
      puts "", "Enter the product type you would like to view"
      print ">> "
      product_type_to_view = gets.chomp.downcase
    end

    @product_list.display_product_type(product_type_to_view)
  end

  def display_shopping_cart
    puts "", "Shopping cart: "
    @shopping_cart.show_cart
  end

end

shopicruit = Shopicruit.new

shopicruit.menu
