require 'byebug'

class ShoppingCart
  MAX_WEIGHT_IN_KG = 100

  def initialize
    @cart = []
  end

  def add_to_cart(items)
    items_weight = items[:variants].reduce(0) do |weight, variant|
      weight + variant[:weight]
    end

    if (items_weight + cart_weight) < MAX_WEIGHT_IN_KG
      @cart << items 
    else
      puts "Sorry, you can only carry #{MAX_WEIGHT_IN_KG}kg"
    end

  end

  def show_cart
    price = 0

    @cart.each do |product|
      puts "", product[:product]
      product[:variants].each do |variant|
        puts "", "#{variant[:colour]}, $#{variant[:price]}, #{variant[:weight]}kg"
        price += variant[:price]
      end
    end

    puts "", "Shoppping cart stats: ", ""
    puts "Price: $#{price}"
    puts "Weight: #{cart_weight}kg"

  end

  def cart_weight
    cart_weight = 0

    @cart.each do |product|
      product[:variants].each do |variant|
        cart_weight += variant[:weight]
      end
    end

    return cart_weight

  end

end
