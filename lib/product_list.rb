require_relative './product'

class ProductList

  attr_reader :data

  def initialize(data, cart)
    @data = data
    @cart = cart
  end

  def generate_product_list
    product_list = pass_data_to_formatting
    types = {}

    product_list.each do |product| 
      if !types.key? product.product_type
        types[product.product_type] = [product]
      else
        types[product.product_type] << product
      end
    end

    return types.keys.sort

  end

  def display_product_list
    generate_product_list.each do |product_type|
      puts "", "| #{product_type}"
    end
  end

  def display_product_type(product_type_to_view)
    selected_type = @products.select {|product| product.product_type == product_type_to_view}
    product_entry = 0

    selected_type.each do |item|
      product_entry += 1
      puts "", "#{product_entry}. #{item.title}"
    end

    puts "", "Choose the product you would like to view by entering its number"
    print ">> "
    product_to_view = (gets.chomp.to_i - 1)
    display_product_variants(selected_type[product_to_view].title)

  end

  def display_product_variants(product_to_view)
    selected_product = @products.find {|product| product.title == product_to_view}
    variant_entry = 0

    puts "", "Options for #{selected_product.title}"

    selected_product.variants.each do |variant|
      variant_entry += 1
      puts "", "#{variant_entry}. #{variant[:colour]} (Price: $#{variant[:price]} Weight: #{variant[:weight]}kg)"
    end

    add_to_cart(selected_product)

  end

  def add_to_cart(selected_product)
    puts "", "Choose the variants you would like to add to your cart. To select multiple items, enter in each number seperated by a comma"
    print ">> "
    variant_to_cart = gets.chomp
    selected_variants = variant_to_cart.split(",").map{|variant| selected_product.variants[(variant.to_i - 1)]}
    cart_item = {
      product: selected_product.title,
      variants: selected_variants
    }

    @cart.add_to_cart(cart_item)
  end

  def pass_data_to_formatting
    @products = []

    @data.each do |product|
      variants = []
      id = product["id"]
      title = product["title"]
      product_type = product["product_type"].downcase
      variants_of_product = product["variants"]

      variants_of_product.each do |variant|
        colour = variant["title"]
        price = (variant["price"].to_f).round(2)
        weight = (variant["grams"].to_f * 0.001).round(2)
        variants << {colour: colour, price: price, weight: weight}
      end

      @products << Product.new(id, title, product_type, variants)

    end

    return @products
  end

end
