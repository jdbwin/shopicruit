class Product

  attr_reader :id, :title, :product_type, :variants

  def initialize(id, title, product_type, variants)
    @id = id
    @title = title
    @product_type = product_type
    @variants = variants
  end

end
